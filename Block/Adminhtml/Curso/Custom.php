<?php


namespace RaiaDrogasil\Curso\Block\Adminhtml\Curso;

use \Magento\Backend\Block\Template\Context;

use \Magento\Backend\Block\Template;

class Custom extends Template
{

    /**
     * Constructor
     *
     * @param Context  $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
