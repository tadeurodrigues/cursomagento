<?php


namespace RaiaDrogasil\Curso\Block\Curso;

use \Magento\Framework\View\Element\Template\Context;

use Magento\Framework\View\Element\Template;

class Custom extends Template
{

    /**
     * Constructor
     *
     * @param Context  $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}