<?php


namespace RaiaDrogasil\Curso\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use Magento\Backend\App\Action;

abstract class Curso extends Action
{

    const ADMIN_RESOURCE = 'RaiaDrogasil::top_level';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

        /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
        ->addBreadcrumb(__('RaiaDrogasil'), __('RaiaDrogasil'))
        ->addBreadcrumb(__('Curso'), __('Curso'));
        return $resultPage;
    }
}