<?php


namespace RaiaDrogasil\Curso\Controller\Adminhtml\Curso;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Registry;

use RaiaDrogasil\Curso\Controller\Adminhtml\Curso;

class Custom extends Curso
{
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct(
            $context, 
            $resultPageFactory
        );
    }

    /**
     * Index action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->getConfig()->getTitle()->prepend(__('Curso action custom'));
        return $resultPage;
    }
}
