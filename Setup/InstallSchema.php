<?php


namespace RaiaDrogasil\Curso\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        if ($installer->tableExists('raiadrogasil_curso_curso')) {
            return;
        }

        $installer->startSetup();

        $table_raiadrogasil_curso = $installer->getConnection()->newTable($installer->getTable('raiadrogasil_curso_curso'));

        $table_raiadrogasil_curso->addColumn(
            'curso_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_raiadrogasil_curso->addColumn(
            'nome',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'nome'
        );

        $table_raiadrogasil_curso->addColumn(
            'conteudo',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'conteudo'
        );


        $installer->getConnection()->createTable($table_raiadrogasil_curso);

        $installer->endSetup();
    }
}
