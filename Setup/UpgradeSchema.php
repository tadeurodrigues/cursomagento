<?php


namespace RaiaDrogasil\Curso\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

       if (version_compare($context->getVersion(), "1.0.1", "<")) {
           $oldTable = $installer->getTable('raiadrogasil_curso_curso');
           $newTable = $installer->getTable('raiadrogasil_curso');

           $installer->getConnection()->renameTable($oldTable, $newTable);
       }

        if (version_compare($context->getVersion(), "1.0.2", "<")) {
            $table = $installer->getTable('raiadrogasil_curso');

            $installer->getConnection()
                ->addColumn(
                            $table,
                'descricao',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'descricao'
            );
        }

        $installer->endSetup();
    }
}
